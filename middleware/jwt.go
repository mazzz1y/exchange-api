package middleware

import (
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/gin-gonic/gin"
	h "gitlab.com/drubtsov/exchange-api/helpers"
)

func JWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		_, err := request.ParseFromRequest(c.Request, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
			b := ([]byte(h.Env.JWTSecret))
			c.Set("id", token.Claims.(jwt.MapClaims)["jti"])
			c.Set("role", token.Claims.(jwt.MapClaims)["role"])
			return b, nil
		})
		if err != nil {
			h.NewError(c, 401, err, "invalid token")
		}
	}

}
