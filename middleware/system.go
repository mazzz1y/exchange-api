package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/drubtsov/exchange-api/dao"
	h "gitlab.com/drubtsov/exchange-api/helpers"
)

func SystemCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := dao.PQ.Exec("SELECT 1;").Error
		if err != nil {
			h.NewError(c, 500, err, "server error")
			return
		}
	}
}
