package middleware

import (
	"errors"

	"github.com/gin-gonic/gin"
	h "gitlab.com/drubtsov/exchange-api/helpers"
)

type roleStruct struct{}

var Role roleStruct

func (Role roleStruct) User() gin.HandlerFunc {
	return func(c *gin.Context) {
		role, _ := c.Get("role")
		if role != "user" && role != "admin" {
			h.NewError(c, 401, errors.New("role isn't user"), "permission denied")
			return
		}
	}
}

func (Role roleStruct) Admin() gin.HandlerFunc {
	return func(c *gin.Context) {
		role, _ := c.Get("role")
		if role != "admin" {
			h.NewError(c, 401, errors.New("role isn't admin"), "permission denied")
			return
		}
	}
}
