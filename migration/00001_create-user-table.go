package migration

import (
	"database/sql"

	"github.com/pressly/goose"
	"gitlab.com/drubtsov/exchange-api/dao"
	"gitlab.com/drubtsov/exchange-api/models"
)

func init() {
	goose.AddMigration(Up00001, Down00001)
}

func Up00001(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	err := dao.PQ.CreateTable(&models.User{}).Error
	return err
}

func Down00001(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	err := dao.PQ.DropTable(&models.User{}).Error
	return err
}
