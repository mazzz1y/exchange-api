package migration

import (
	"database/sql"

	"github.com/pressly/goose"
	"gitlab.com/drubtsov/exchange-api/dao"
	"gitlab.com/drubtsov/exchange-api/models"
)

func init() {
	goose.AddMigration(Up00005, Down00005)
}

func Up00005(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	err := dao.PQ.CreateTable(&models.Currency{}).Error
	if err != nil {
		return err
	}
	err = dao.PQ.Model(&models.Wallet{}).AddForeignKey("currency_id", "currencies(id)", "CASCADE", "CASCADE").Error
	return err
}

func Down00005(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	err := dao.PQ.DropTable(&models.Currency{}).Error
	return err
}
