package migration

import (
	"database/sql"

	"github.com/pressly/goose"
	"gitlab.com/drubtsov/exchange-api/dao"
	"gitlab.com/drubtsov/exchange-api/models"
)

func init() {
	goose.AddMigration(Up00007, Down00007)
}

func Up00007(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	err := dao.PQ.AutoMigrate(&models.UserFamilies{}).Error
	if err != nil {
		return err
	}
	err = dao.PQ.Model(&models.UserFamilies{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		return err
	}
	err = dao.PQ.Model(&models.UserFamilies{}).AddForeignKey("family_id", "families(id)", "CASCADE", "CASCADE").Error
	return err
}

func Down00007(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	err := dao.PQ.DropTable(&models.UserFamilies{}).Error
	return err
}
