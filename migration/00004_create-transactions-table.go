package migration

import (
	"database/sql"

	"github.com/pressly/goose"
	"gitlab.com/drubtsov/exchange-api/dao"
	"gitlab.com/drubtsov/exchange-api/models"
)

func init() {
	goose.AddMigration(Up00004, Down00004)
}

func Up00004(tx *sql.Tx) error {
	err := dao.PQ.CreateTable(&models.Transaction{}).Error
	if err != nil {
		return err
	}
	err = dao.PQ.Model(&models.Transaction{}).AddForeignKey("wallet_id", "wallets(id)", "CASCADE", "CASCADE").Error
	return err
}

func Down00004(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	err := dao.PQ.DropTable(&models.Transaction{}).Error
	return err
}
