package controllers

import (
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
	h "gitlab.com/drubtsov/exchange-api/helpers"
	"gitlab.com/drubtsov/exchange-api/models"
)

type familyControllers struct{}

var Family familyControllers

// @Summary Create family
// @ID create-family
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param Wallet body models.Family true "Family model"
// @Success 200 {object} models.Family
// @Failure 401 {object} helpers.HTTPError
// @Router /families [post]
func (*familyControllers) Create(c *gin.Context) {
	var family models.Family
	if err := c.BindJSON(&family); err != nil {
		h.NewError(c, 400, err, "invalid family")
		return
	}
	family.OwnerID = h.GetUintID(c)
	family, _ = family.Create()

	//TODO check if already exist
	c.JSON(200, family)
}

// @Summary Update family
// @ID update-family
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param Family body models.Family true "Family model"
// @Success 200 {object} models.Family
// @Failure 401 {object} helpers.HTTPError
// @Router /families/{id} [post]
func (*familyControllers) Update(c *gin.Context) {
	id, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid id")
		return
	}
	var family models.Family
	if err := c.BindJSON(&family); err != nil {
		h.NewError(c, 400, err, "invalid family")
		return
	}
	family.ID = uint(id)
	if family.OwnerID != h.GetUintID(c) {
		h.NewError(c, 403, errors.New("permission denied"), "permission denied")
		return
	}
	family, _ = family.Update()
	c.JSON(200, family)
}

// @Summary Get family
// @ID get-family
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param id path int true "Family ID"
// @Success 200 {object} models.Family
// @Failure 401 {object} helpers.HTTPError
// @Router /families/{id} [get]
func (*familyControllers) Get(c *gin.Context) {
	id, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid id")
		return
	}

	family := models.Family{ID: uint(id)}

	err = family.CheckUserExist(h.GetUintID(c))
	if err != nil {
		h.NewError(c, 404, err, "family not found")
		return
	}
	family, _ = family.Get()
	c.JSON(200, family)
}

// @Summary Delete family
// @ID delete-family
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param id path int true "Family ID"
// @Success 200 {object} models.Family
// @Failure 400 {object} helpers.HTTPError
// @Failure 403 {object} helpers.HTTPError
// @Router /families/{id} [delete]
func (*familyControllers) Delete(c *gin.Context) {
	id, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid id")
		return
	}
	family, err := models.Family{ID: uint(id)}.Get()
	if family.OwnerID != uint(h.GetUintID(c)) {
		h.NewError(c, 403, err, "permission denied")
		return
	}
	family, _ = family.Delete()
	h.NewStatus(c, "deleted")
}

// @Summary Get all families
// @ID get-all-familyies
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Success 200 {array} models.Family
// @Failure 404 {object} helpers.HTTPError
// @Router /families [get]
func (*familyControllers) GetAll(c *gin.Context) {
	families, err := models.User{ID: uint(h.GetUintID(c))}.GetFamilies()
	if err != nil || len(families) == 0 {
		h.NewError(c, 404, err, "families not found")
		return
	}
	c.JSON(200, families)
}

// @Summary Get all users from family
// @ID get-all-users-from-family
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Success 200 {array} models.User
// @Failure 401 {object} helpers.HTTPError
// @Router /families/{id}/users [get]
func (*familyControllers) GetUsers(c *gin.Context) {
	familyID, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid family id")
		return
	}
	family, err := models.Family{ID: uint(familyID)}.Get()
	if err != nil || family.OwnerID != uint(h.GetUintID(c)) {
		h.NewError(c, 403, err, "permission denied")
		return
	}
	users, _ := family.GetUsers()
	c.JSON(200, users)
}

// @Summary Add user to family
// @ID add-user-to-family
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param User body models.User true "User model"
// @Success 200 {array} models.User
// @Failure 400 {object} helpers.HTTPError
// @Router /families/{id}/users [post]
func (*familyControllers) AddUser(c *gin.Context) {
	familyID, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid family id")
		return
	}

	var user models.User
	err = c.BindJSON(&user)
	if user, err = user.Get(); err != nil || user.ID == 0 {
		h.NewError(c, 400, err, "invalid user")
		return
	}

	family, err := models.Family{ID: uint(familyID)}.Get()
	if err != nil || family.OwnerID != uint(h.GetUintID(c)) {
		h.NewError(c, 403, err, "permission denied")
		return
	}
	if err := family.CheckUserExist(user.ID); err == nil {
		h.NewError(c, 400, err, "user already in family")
	}

	family.AddUser(models.User{ID: user.ID})

	go h.EmailSender.DialAndSend(h.FamilyInviteEmail(user.Email, family.ID))
	h.NewStatus(c, "invite sent")
}

// @Summary Delete user from family
// @ID delete user from family
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Success 200 {object} helpers.HTTPStatus
// @Failure 401 {object} helpers.HTTPError
// @Router /families/{id}/users/{user_id} [delete]
func (*familyControllers) DeleteUser(c *gin.Context) {
	familyIDString := c.Param("id")
	userIDString := c.Param("user_id")
	familyID, err := strconv.ParseUint(familyIDString, 10, 64)
	if err != nil {
		h.NewError(c, 400, err, "invalid family id")
		return
	}
	userID, err := strconv.ParseUint(userIDString, 10, 64)
	if err != nil {
		h.NewError(c, 400, err, "invalid user id")
		return
	}
	family, err := models.Family{ID: uint(familyID)}.Get()
	if err != nil || family.OwnerID != uint(h.GetUintID(c)) {
		h.NewError(c, 403, err, "permission denied")
		return
	}
	// Check if user/family doesnt exist ??
	family.DeleteUser(models.User{ID: uint(userID)})
	h.NewStatus(c, "deleted")
}

// @Summary Confirm family invite
// @ID confirm-family-invite
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Success 200 {object} helpers.HTTPStatus
// @Failure 401 {object} helpers.HTTPError
// @Failure 403 {object} helpers.HTTPError
// @Router /families/{id} [post]
func (*familyControllers) ConfirmUser(c *gin.Context) {
	familyID, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid family id")
		return
	}

	user, _ := models.User{ID: uint(h.GetUintID(c))}.Get()
	family, _ := models.Family{ID: uint(familyID)}.Get()

	if err := family.CheckUserExist(user.ID); err != nil {
		h.NewError(c, 403, err, "permission denied")
		return
	}
	err = family.ConfirmUser(user)
	if err != nil {
		h.NewError(c, 403, err, "user already confirmed")
		return
	}
	h.NewStatus(c, "confirmed")
}
