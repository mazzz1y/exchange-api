package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"
	"gitlab.com/drubtsov/exchange-api/models"
)

func TestCurrencyCreate(t *testing.T) {
	owner := initTestAdmin()
	currency := models.Currency{
		Name: fake.Currency(),
	}

	jsonCurrency, _ := json.Marshal(currency)

	w := PerformRequest(
		"POST",
		"/currencies",
		owner,
		bytes.NewBuffer(jsonCurrency),
	)

	var createdCurrency models.Currency
	json.Unmarshal(([]byte(w.Body.String())), &createdCurrency)

	createdCurrency, err := models.Currency{ID: createdCurrency.ID}.Get()
	assert.Nil(t, err)
	jsonCurrency, err = json.Marshal(createdCurrency)
	assert.Nil(t, err)

	assert.Equal(t, string(jsonCurrency), w.Body.String())
}

func TestCurrencyCreateInvalidForm(t *testing.T) {
	form := "invalid_form"
	owner := initTestAdmin()

	w := PerformRequest(
		"POST",
		"/currencies",
		owner,
		bytes.NewBuffer([]byte(form)),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

// func TestCurrencyGet(t *testing.T) {
// 	owner := initTestUser()
// 	currency := initTestCurrency(owner.ID)
// 	jsonCurrency, _ := json.Marshal(currency)

// 	w := PerformRequest(
// 		"GET",
// 		"/currencies/"+fmt.Sprint(currency.ID),
// 		owner,
// 		nil,
// 	)

// 	assert.Equal(t, http.StatusOK, w.Code)
// 	assert.Equal(t, string(jsonCurrency), w.Body.String())
// }

func TestCurrencyGetAll(t *testing.T) {
	models.Currency{}.DropAll()
	owner := initTestUser()

	var fArr [3]models.Currency
	for i := 0; i < 3; i++ {
		fArr[i] = initTestCurrency(owner.ID)
	}
	jsonCreatedFamilies, err := json.Marshal(fArr[:])

	w := PerformRequest(
		"GET",
		"/currencies",
		owner,
		nil,
	)

	assert.Nil(t, err)
	assert.Equal(t, string(jsonCreatedFamilies), w.Body.String())
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestCurrencyGetAllNotFound(t *testing.T) {
	models.Currency{}.DropAll()
	owner := initTestUser()

	w := PerformRequest(
		"GET",
		"/currencies",
		owner,
		nil,
	)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestCurrencyDelete(t *testing.T) {
	owner := initTestAdmin()
	currency, _ := models.Currency{
		Name: fake.Currency(),
	}.Create()

	w := PerformRequest(
		"DELETE",
		"/currencies/"+fmt.Sprint(currency.ID),
		owner,
		nil,
	)
	assert.Equal(t, w.Code, 200)
}

func initTestCurrency(ownerID uint) models.Currency {
	f, _ := models.Currency{
		Name: fake.Currency(),
	}.Create()
	return f
}
