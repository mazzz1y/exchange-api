package controllers

import (
	"errors"

	"github.com/gin-gonic/gin"
	h "gitlab.com/drubtsov/exchange-api/helpers"
	"gitlab.com/drubtsov/exchange-api/models"
)

type walletControllers struct{}

var Wallet walletControllers

// @Summary Create wallet
// @ID create-wallet
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param User body models.Wallet true "Wallet model"
// @Success 200 {object} models.Wallet
// @Failure 401 {object} helpers.HTTPError
// @Failure 500 {object} helpers.HTTPError
// @Router /wallets [post]
func (*walletControllers) Create(c *gin.Context) {
	var wallet models.Wallet
	if err := c.BindJSON(&wallet); err != nil {
		h.NewError(c, 401, err, "invalid wallet")
		return
	}
	wallet.OwnerID = h.GetUintID(c)
	wallet, _ = wallet.Create()
	c.JSON(200, wallet)
}

// @Summary Get wallet
// @ID get-wallet
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param id path int true "Wallet ID"
// @Success 200 {object} models.Wallet
// @Failure 401 {object} helpers.HTTPError
// @Failure 500 {object} helpers.HTTPError
// @Router /wallets/{id} [get]
func (*walletControllers) Get(c *gin.Context) {
	id, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid id")
		return
	}
	var wallet models.Wallet
	wallet.ID = uint(id)
	wallet, err = wallet.Get()

	allowed := checkIsUserAllowed(wallet, c)
	if allowed {
		c.JSON(200, wallet)
	} else {
		h.NewError(c, 404, err, "wallet not found")
	}
}

// @Summary Update wallet
// @ID update-wallet
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param User body models.Wallet true "Wallet model"
// @Param id path int true "Wallet ID"
// @Success 200 {object} models.Wallet
// @Failure 401 {object} helpers.HTTPError
// @Failure 500 {object} helpers.HTTPError
// @Router /wallets/{id} [put]
func (*walletControllers) Update(c *gin.Context) {
	id, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid id")
		return
	}
	var wallet models.Wallet
	if err := c.BindJSON(&wallet); err != nil {
		h.NewError(c, 401, err, "invalid wallet")
		return
	}

	wallet.ID = uint(id)
	if wallet.OwnerID != h.GetUintID(c) {
		h.NewError(c, 403, errors.New("wallet.OwnerID != userID"), "permission denied")
		return
	}
	wallet, _ = wallet.Update()
	c.JSON(200, wallet)
}

// @Summary Delete wallet
// @ID delete-wallet
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Success 200 {object} helpers.HTTPStatus
// @Failure 401 {object} helpers.HTTPError
// @Failure 404 {object} helpers.HTTPError
// @Router /wallets/{id} [delete]
func (*walletControllers) Delete(c *gin.Context) {
	id, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid id")
		return
	}
	userID := h.GetUintID(c)
	wallet, err := models.Wallet{ID: uint(id)}.Get()
	if err != nil {
		h.NewError(c, 404, err, "wallet not found")
		return
	}
	if wallet.OwnerID != userID {
		h.NewError(c, 403, err, "permission denied")
		return
	}
	wallet.Delete()
	h.NewStatus(c, "deleted")
}

// @Summary Get all user wallets
// @ID get-all-user-wallets
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Success 200 {array} models.Wallet
// @Failure 401 {object} helpers.HTTPError
// @Failure 500 {object} helpers.HTTPError
// @Router /wallets [GET]
func (*walletControllers) GetAll(c *gin.Context) {
	userID := h.GetUintID(c)
	wArr, _ := models.Wallet{OwnerID: userID}.GetAll()
	c.JSON(200, wArr)
}

func checkIsUserAllowed(w models.Wallet, c *gin.Context) bool {
	allowedUsers, _ := w.GetUsers()
	currentUserID := h.GetUintID(c)
	for _, u := range allowedUsers {
		if u.ID == currentUserID {
			return true
		}
	}
	return false
}
