package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	h "gitlab.com/drubtsov/exchange-api/helpers"
	"gitlab.com/drubtsov/exchange-api/models"
)

func TestAuthRegister(t *testing.T) {
	authForm := initTestAuthFrom()
	jsonAuthForm, _ := json.Marshal(authForm)

	w := PerformRequest(
		"POST",
		"/auth/register",
		models.User{},
		bytes.NewBuffer(jsonAuthForm),
	)

	var userFromRequest models.User
	err := json.Unmarshal([]byte(w.Body.String()), &userFromRequest)
	assert.Nil(t, err)
	createdUser, _ := models.User{ID: userFromRequest.ID}.Get()

	h.CompareHashAndString(authForm.Password, createdUser.PassHash)
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Nil(t, err)
}

func TestAuthRegisterExistUser(t *testing.T) {
	authForm := initTestAuthFrom()

	models.User{
		Email:    authForm.Email,
		PassHash: "123",
	}.New()

	jsonAuthForm, _ := json.Marshal(authForm)

	w := PerformRequest(
		"POST",
		"/auth/register",
		models.User{},
		bytes.NewBuffer(jsonAuthForm),
	)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}

func TestAuthRegisterInvalidBody(t *testing.T) {
	authForm := "invalid_body"

	w := PerformRequest(
		"POST",
		"/auth/register",
		models.User{},
		bytes.NewBuffer([]byte(authForm)),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestAuthLogin(t *testing.T) {
	authForm := initTestAuthFrom()

	passHash := h.GenerateHash(authForm.Password)

	models.User{
		Email:       authForm.Email,
		PassHash:    string(passHash),
		IsConfirmed: true,
	}.New()

	jsonAuthForm, _ := json.Marshal(authForm)

	w := PerformRequest(
		"POST",
		"/auth/login",
		models.User{},
		bytes.NewBuffer(jsonAuthForm),
	)

	var jwtResponse h.JWTResponse
	err := json.Unmarshal([]byte(w.Body.String()), &jwtResponse)
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, w.Code)
	assert.True(t, validateJWTToken(jwtResponse.Token))
}

func TestAuthLoginInvalidPassword(t *testing.T) {
	authForm := initTestAuthFrom()

	passHash := h.GenerateHash(authForm.Password)

	models.User{
		Email:       authForm.Email,
		PassHash:    string(passHash),
		IsConfirmed: true,
	}.New()

	authForm.Password = "invalid_password"

	jsonAuthForm, _ := json.Marshal(authForm)

	w := PerformRequest(
		"POST",
		"/auth/login",
		models.User{},
		bytes.NewBuffer(jsonAuthForm),
	)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}

func TestAuthLoginInvalidLogin(t *testing.T) {
	authForm := initTestAuthFrom()

	passHash := h.GenerateHash(authForm.Password)

	models.User{
		Email:    authForm.Email,
		PassHash: string(passHash),
	}.New()

	authForm.Email = "invalid_login"

	jsonAuthForm, _ := json.Marshal(authForm)

	w := PerformRequest(
		"POST",
		"/auth/login",
		models.User{},
		bytes.NewBuffer(jsonAuthForm),
	)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}

func TestAuthLoginInvalidForm(t *testing.T) {
	authForm := "invalid_form"
	w := PerformRequest(
		"POST",
		"/auth/login",
		models.User{},
		bytes.NewBuffer([]byte(authForm)),
	)
	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestAuthConfirm(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: "123",
	}.New()

	token := h.MakeEmailVerificationToken(u.Email)

	form := TokenForm{
		Token: token,
	}

	tokenForm, _ := json.Marshal(form)

	w := PerformRequest(
		"POST",
		"/auth/confirmation",
		models.User{},
		bytes.NewBuffer(tokenForm),
	)

	u, _ = u.Get()

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, true, u.IsConfirmed)
}

func TestAuthConfirmInvalidBody(t *testing.T) {
	form := "invalid_body"

	tokenForm, _ := json.Marshal(form)

	w := PerformRequest(
		"POST",
		"/auth/confirmation",
		models.User{},
		bytes.NewBuffer(tokenForm),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestAuthConfirmInvalidToken(t *testing.T) {
	token := "invalid_token"

	form := TokenForm{
		Token: token,
	}

	tokenForm, _ := json.Marshal(form)

	w := PerformRequest(
		"POST",
		"/auth/confirmation",
		models.User{},
		bytes.NewBuffer(tokenForm),
	)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}

func TestAuthConfirmTokenAlreadyUsed(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: "123",
	}.New()

	token := h.MakeEmailVerificationToken(u.Email)

	form := TokenForm{
		Token: token,
	}

	tokenForm, _ := json.Marshal(form)

	w := PerformRequest(
		"POST",
		"/auth/confirmation",
		models.User{},
		bytes.NewBuffer(tokenForm),
	)

	w = PerformRequest(
		"POST",
		"/auth/confirmation",
		models.User{},
		bytes.NewBuffer(tokenForm),
	)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}

func initTestAuthFrom() AuthForm {
	return AuthForm{
		Email:    fake.EmailAddress(),
		Password: fake.SimplePassword(),
	}
}

func validateJWTToken(tokenString string) bool {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(h.Env.JWTSecret), nil
	})

	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid && err == nil {
		return true
	}
	return false
}
