package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"
	"gitlab.com/drubtsov/exchange-api/models"
)

func TestUserGet(t *testing.T) {
	user := initTestUser()
	userJSON, _ := json.Marshal(user)

	w := PerformRequest(
		"GET",
		"/users/"+fmt.Sprint(user.ID),
		user,
		nil,
	)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, string(userJSON), w.Body.String())
}

func TestUserGetInvalidID(t *testing.T) {
	user := initTestUser()
	id := "invalid_id"

	w := PerformRequest(
		"GET",
		"/users/"+id,
		user,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestUserGetNotOwner(t *testing.T) {
	owner := initTestUser()
	user := initTestUser()

	w := PerformRequest(
		"GET",
		"/users/"+fmt.Sprint(owner.ID),
		user,
		nil,
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func TestUserDelete(t *testing.T) {
	user := initTestUser()

	w := PerformRequest(
		"DELETE",
		"/users/"+fmt.Sprint(user.ID),
		user,
		nil,
	)

	deletedUser, _ := user.Get()
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, deletedUser.IsDeleted, true)
}

func TestUserDeleteInvalidID(t *testing.T) {
	user := initTestUser()

	id := "invalid_id"

	w := PerformRequest(
		"DELETE",
		"/users/"+id,
		user,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestUserDeleteNotOwner(t *testing.T) {
	owner := initTestUser()
	user := initTestUser()

	w := PerformRequest(
		"DELETE",
		"/users/"+fmt.Sprint(owner.ID),
		user,
		nil,
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func initTestUser() models.User {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()
	return u
}

func initTestAdmin() models.User {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
		Role:     "admin",
	}.New()
	return u
}
