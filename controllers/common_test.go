package controllers

import (
	"io"
	"net/http"
	"net/http/httptest"

	"gitlab.com/drubtsov/exchange-api/models"
)

func PerformRequest(method string, path string, user models.User, body io.Reader) *httptest.ResponseRecorder {
	r := InitGin()

	req, _ := http.NewRequest(method, path, body)
	user.AuthorizeForRequest(req)

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	return w
}
