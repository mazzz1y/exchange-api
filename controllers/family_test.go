package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/drubtsov/exchange-api/models"
)

func TestFamilyCreate(t *testing.T) {
	owner := initTestUser()
	family := models.Family{
		Name:    fake.Word(),
		OwnerID: owner.ID,
	}

	jsonFamily, _ := json.Marshal(family)

	w := PerformRequest(
		"POST",
		"/families",
		owner,
		bytes.NewBuffer(jsonFamily),
	)

	var createdFamily models.Family
	json.Unmarshal(([]byte(w.Body.String())), &createdFamily)

	createdFamily, err := models.Family{ID: createdFamily.ID}.Get()
	assert.Nil(t, err)
	jsonFamily, err = json.Marshal(createdFamily)
	assert.Nil(t, err)

	assert.Equal(t, string(jsonFamily), w.Body.String())
}

func TestFamilyCreateInvalidForm(t *testing.T) {
	form := "invalid_form"
	owner := initTestUser()

	w := PerformRequest(
		"POST",
		"/families",
		owner,
		bytes.NewBuffer([]byte(form)),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyGet(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)
	jsonFamily, _ := json.Marshal(family)

	w := PerformRequest(
		"GET",
		"/families/"+fmt.Sprint(family.ID),
		owner,
		nil,
	)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, string(jsonFamily), w.Body.String())
}

func TestFamilyGetInvalidID(t *testing.T) {
	owner := initTestUser()
	id := "invalid_id"

	w := PerformRequest(
		"GET",
		"/families/"+id,
		owner,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyGetNotFound(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)

	w := PerformRequest(
		"GET",
		"/families/"+fmt.Sprint(family.ID+1),
		owner,
		nil,
	)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestFamilyDelete(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)

	w := PerformRequest(
		"DELETE",
		"/families/"+fmt.Sprint(family.ID),
		owner,
		nil,
	)
	//TODO
	family, err := family.Get()
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, true, family.IsDeleted)
}

func TestFamilyDeleteInvalidID(t *testing.T) {
	owner := initTestUser()
	id := "invalid_id"

	w := PerformRequest(
		"DELETE",
		"/families/"+id,
		owner,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyDeleteNotOwner(t *testing.T) {
	owner := initTestUser()
	user := initTestUser()
	family := initTestFamily(owner.ID)

	w := PerformRequest(
		"DELETE",
		"/families/"+fmt.Sprint(family.ID),
		user,
		nil,
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func TestFamilyUpdate(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)

	oldName := family.Name
	family.Name = fake.Word()

	jsonFamily, _ := json.Marshal(family)

	w := PerformRequest(
		"PUT",
		"/families/"+fmt.Sprint(family.ID),
		owner,
		bytes.NewBuffer(jsonFamily),
	)

	updatedFamily, err := models.Family{ID: family.ID}.Get()
	assert.Nil(t, err)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotEqual(t, oldName, updatedFamily.Name, "they shouldn't be equal")
}

func TestFamilyUpdateInvalidID(t *testing.T) {
	owner := initTestUser()
	id := "invalid id"

	w := PerformRequest(
		"PUT",
		"/families/"+id,
		owner,
		bytes.NewBuffer([]byte("{}")),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyUpdateInvalidBody(t *testing.T) {
	owner := initTestUser()
	family := "invalid_body"

	w := PerformRequest(
		"PUT",
		"/families/"+"5",
		owner,
		bytes.NewBuffer([]byte(family)),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyUpdateNotOwner(t *testing.T) {
	user := initTestUser()
	owner := initTestUser()
	family := initTestFamily(owner.ID)

	jsonFamily, _ := json.Marshal(family)

	w := PerformRequest(
		"PUT",
		"/families/"+fmt.Sprint(family.ID),
		user,
		bytes.NewBuffer(jsonFamily),
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func TestFamilyGetUsers(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)

	var userArr [4]models.User
	userArr[0] = owner
	for i := 1; i < 4; i++ {
		userArr[i] = initTestUser()
		family.AddUser(userArr[i])
	}
	jsonCreatedUsers, err := json.Marshal(userArr[:])

	w := PerformRequest(
		"GET",
		"/families/"+fmt.Sprint(family.ID)+"/users",
		owner,
		nil,
	)

	assert.Nil(t, err)
	assert.Equal(t, string(jsonCreatedUsers), w.Body.String())
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestFamilyGetUsersInvalidID(t *testing.T) {
	owner := initTestUser()
	id := "invalid_id"

	w := PerformRequest(
		"GET",
		"/families/"+id+"/users",
		owner,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyGetUsersNotOwner(t *testing.T) {
	owner := initTestUser()
	user := initTestUser()
	family := initTestFamily(owner.ID)

	w := PerformRequest(
		"GET",
		"/families/"+fmt.Sprint(family.ID)+"/users",
		user,
		nil,
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func TestFamilyAddUser(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)
	newUser := initTestUser()

	jsonNewUser, _ := json.Marshal(newUser)

	w := PerformRequest(
		"POST",
		"/families/"+fmt.Sprint(family.ID)+"/users",
		owner,
		bytes.NewBuffer(jsonNewUser),
	)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestFamilyAddUserInvalidID(t *testing.T) {
	owner := initTestUser()

	id := "invalid_id"

	w := PerformRequest(
		"POST",
		"/families/"+id+"/users",
		owner,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyAddUserInvalidBody(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)

	jsonNewUser := []byte("invalid_body")

	w := PerformRequest(
		"POST",
		"/families/"+fmt.Sprint(family.ID)+"/users",
		owner,
		bytes.NewBuffer(jsonNewUser),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyAddUserNotOwner(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)
	user := initTestUser()
	newUser := initTestUser()

	jsonNewUser, _ := json.Marshal(newUser)

	w := PerformRequest(
		"POST",
		"/families/"+fmt.Sprint(family.ID)+"/users",
		user,
		bytes.NewBuffer(jsonNewUser),
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func TestFamilyAddUserInvalidUser(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)

	jsonNewUser, _ := json.Marshal(models.User{
		Email: "invalid_email",
	})

	w := PerformRequest(
		"POST",
		"/families/"+fmt.Sprint(family.ID)+"/users",
		owner,
		bytes.NewBuffer(jsonNewUser),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyAddUserAlreadyExist(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)

	jsonNewUser, _ := json.Marshal(owner)

	w := PerformRequest(
		"POST",
		"/families/"+fmt.Sprint(family.ID)+"/users",
		owner,
		bytes.NewBuffer(jsonNewUser),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyGetAll(t *testing.T) {
	owner := initTestUser()

	var fArr [3]models.Family
	for i := 0; i < 3; i++ {
		fArr[i] = initTestFamily(owner.ID)
	}
	jsonCreatedFamilies, err := json.Marshal(fArr[:])

	w := PerformRequest(
		"GET",
		"/families",
		owner,
		nil,
	)

	assert.Nil(t, err)
	assert.Equal(t, string(jsonCreatedFamilies), w.Body.String())
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestFamilyGetAllNotFound(t *testing.T) {
	owner := initTestUser()

	w := PerformRequest(
		"GET",
		"/families",
		owner,
		nil,
	)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestFamilyDeleteUser(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)
	newUser := initTestUser()

	family.AddUser(newUser)

	w := PerformRequest(
		"DELETE",
		"/families/"+fmt.Sprint(family.ID)+"/users/"+fmt.Sprint(newUser.ID),
		owner,
		nil,
	)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestFamilyDeleteUserInvalidFamilyID(t *testing.T) {
	owner := initTestUser()
	familyID := "invalid_id"

	w := PerformRequest(
		"DELETE",
		"/families/"+familyID+"/users/5",
		owner,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyDeleteUserInvalidUserID(t *testing.T) {
	owner := initTestUser()
	userID := "invalid_id"

	w := PerformRequest(
		"DELETE",
		"/families/5"+"/users/"+userID,
		owner,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyDeleteUserNotOwner(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)
	user := initTestUser()

	family.AddUser(user)

	w := PerformRequest(
		"DELETE",
		"/families/"+fmt.Sprint(family.ID)+"/users/"+fmt.Sprint(user.ID),
		user,
		nil,
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func TestFamilyConfirmUser(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)
	user := initTestUser()

	family.AddUser(user)

	w := PerformRequest(
		"POST",
		"/families/"+fmt.Sprint(family.ID),
		user,
		nil,
	)

	uf, _ := family.GetUserRelation(user)

	assert.Equal(t, true, uf.Confirmed)
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestFamilyConfirmUserInvalidFamily(t *testing.T) {
	user := initTestUser()

	familyID := "invalid_id"

	w := PerformRequest(
		"POST",
		"/families/"+familyID,
		user,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestFamilyConfirmUserInvalidUser(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)
	user := initTestUser()

	family.AddUser(user)

	w := PerformRequest(
		"POST",
		"/families/"+fmt.Sprint(family.ID),
		initTestUser(),
		nil,
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func TestFamilyAlreadyConfirmed(t *testing.T) {
	owner := initTestUser()
	family := initTestFamily(owner.ID)
	user := initTestUser()

	family.AddUser(user)

	w := PerformRequest(
		"POST",
		"/families/"+fmt.Sprint(family.ID),
		user,
		nil,
	)

	w = PerformRequest(
		"POST",
		"/families/"+fmt.Sprint(family.ID),
		user,
		nil,
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func initTestFamily(ownerID uint) models.Family {
	f, _ := models.Family{
		Name:    fake.Word(),
		OwnerID: ownerID,
	}.Create()
	return f
}
