package dao

import (
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	h "gitlab.com/drubtsov/exchange-api/helpers"
)

var PQ = DBConnect()

func DBConnect() *gorm.DB {
	h.LoadEnv()
	db, err := gorm.Open("postgres",
		"host="+h.Env.DbHost+
			" port="+h.Env.DbPort+
			" user="+h.Env.DbUser+
			" dbname="+h.Env.DbName+
			" password="+h.Env.DbPass+
			" sslmode=disable")

	// Setup logger
	db.SetLogger(h.Log)

	if err != nil {
		h.Log.Fatal("Database unavailable: " + err.Error())
	}
	return db
}
