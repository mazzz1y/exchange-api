package models

import "gitlab.com/drubtsov/exchange-api/dao"

type Currency struct {
	ID   uint   `json:"id" gorm:"primary_key;unique"`
	Name string `json:"name"`
}

func (Currency) TableName() string {
	return "currencies"
}

func (c Currency) Create() (Currency, error) {
	c.ID = 0
	err := dao.PQ.Create(&c).Error
	return c, err
}

func (c Currency) Get() (Currency, error) {
	err := dao.PQ.Find(&c, c.ID).Error
	return c, err
}

func (c Currency) Delete() error {
	err := dao.PQ.Model(&c).Delete(&c).Error
	return err
}

func (c Currency) GetAll() ([]Currency, error) {
	var cArr []Currency
	err := dao.PQ.Model(&c).Find(&cArr).Error
	return cArr, err
}

func (Currency) DropAll() error {
	err := dao.PQ.Exec("TRUNCATE TABLE currencies CASCADE;").Error
	return err
}
