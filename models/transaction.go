package models

import "time"

type Transaction struct {
	ID       uint      `json:"id" gorm:"primary_key;unique"`
	WalletID uint      `json:"wallet_id"`
	Value    uint      `json:"value"`
	Date     time.Time `json:"date"`
}

func (Transaction) TableName() string {
	return "transactions"
}
