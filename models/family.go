package models

import (
	"errors"

	"gitlab.com/drubtsov/exchange-api/dao"
)

type Family struct {
	ID        uint     `json:"id" gorm:"primary_key;unique_index"`
	OwnerID   uint     `json:"owner_id"`
	Name      string   `json:"name"`
	IsDeleted bool     `json:"-" gorm:"default:'false'"`
	Wallets   []Wallet `json:"-" gorm:"foreignkey:FamilyID;association_foreignkey:ID"`
	Users     []User   `json:"-" gorm:"auto_preload;many2many:user_families;"`
}

func (Family) TableName() string {
	return "families"
}

func (f Family) Create() (Family, error) {
	f.ID = 0
	err := dao.PQ.Create(&f).Association("Users").Append(&User{ID: f.OwnerID}).Error
	return f, err
}

func (f Family) Get() (Family, error) {
	err := dao.PQ.Find(&f, f.ID).Error
	return f, err
}

func (f Family) GetUserRelation(u User) (UserFamilies, error) {
	var uf UserFamilies
	err := dao.PQ.Where("family_id = ? AND user_id >= ?", f.ID, u.ID).Find(&uf).Error
	return uf, err
}

func (f Family) GetUsers() ([]User, error) {
	var userArr []User
	err := dao.PQ.Model(&f).Association("Users").Find(&userArr).Error
	return userArr, err
}

func (f Family) CheckUserExist(id uint) error {
	var u User
	err := dao.PQ.Model(&f).Related(&u, "Users").Find(&u, id).Error
	return err
}

func (f Family) ConfirmUser(u User) error {
	var uf UserFamilies
	err := dao.PQ.Where("family_id = ? AND user_id >= ?", f.ID, u.ID).Find(&uf).Error
	if err != nil {
		return err
	}
	if uf.Confirmed == true {
		return errors.New("already confirmed")
	}
	uf.Confirmed = true
	err = dao.PQ.Save(&uf).Error
	return err
}

func (f Family) Update() (Family, error) {
	err := dao.PQ.Save(&f).Error
	return f, err
}

func (f Family) AddUser(u User) (User, error) {
	err := dao.PQ.Model(&f).Association("Users").Append(&u).Error
	return u, err
}

func (f Family) DeleteUser(u User) (User, error) {
	err := dao.PQ.Model(&u).Association("Families").Delete(&f).Error
	return u, err
}

func (f Family) Delete() (Family, error) {
	f.IsDeleted = true
	err := dao.PQ.Save(&f).Error
	return f, err
}
