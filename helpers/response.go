package helpers

import (
	"github.com/gin-gonic/gin"
)

type HTTPError struct {
	Error string `json:"error" example:"bad request"`
}

type HTTPStatus struct {
	Status string `json:"status" example:"ok"`
}

func NewError(c *gin.Context, code int, err error, prettyResponse string) {
	e := HTTPError{
		Error: prettyResponse,
	}
	if err != nil {
		c.Error(err)
	}

	c.AbortWithStatusJSON(code, e)
}

func NewStatus(c *gin.Context, statusString string) {
	s := HTTPStatus{
		Status: statusString,
	}
	c.JSON(200, s)
}
