package helpers

import "github.com/sirupsen/logrus"

var Log = createLogger()

func createLogger() *logrus.Logger {
	// Set Log Formatter
	switch Env.LogFormat {
	case "json":
		logrus.SetFormatter(&logrus.JSONFormatter{})
	case "text":
		logrus.SetFormatter(&logrus.TextFormatter{})
	default:
		logrus.SetFormatter(&logrus.TextFormatter{})
		logrus.Info("Unknown log formatter, using text")
	}

	// Set Log Level
	switch Env.LogLevel {
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
	case "panic":
		logrus.SetLevel(logrus.PanicLevel)
	default:
		logrus.SetLevel(logrus.InfoLevel)
		logrus.Info("Unknown log level, using info level")
	}
	return logrus.StandardLogger()
}
