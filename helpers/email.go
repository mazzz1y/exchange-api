package helpers

import (
	"fmt"
	"strconv"

	gomail "gopkg.in/gomail.v2"
)

var EmailSender = gomail.NewDialer(Env.MailHost, getMailPortInt(), Env.MailUser, Env.MailPass)

func getMailPortInt() int {
	port, err := strconv.Atoi(Env.MailPort)
	if err != nil {
		Log.Fatal("Incorrect email port: \"" + Env.MailPort + "\"")
	}
	return port
}

func RegistrationEmail(email string) *gomail.Message {
	hash := MakeEmailVerificationToken(email)

	//TODO send link to frontend
	url := Env.AppURL + "/auth/confirmation/" + hash

	m := gomail.NewMessage()
	m.SetHeader("From", Env.MailSender)
	m.SetHeader("To", email)
	m.SetHeader("Subject", "Verify Registration")
	m.SetBody("text/html", "POST "+url)
	return m
}

func FamilyInviteEmail(email string, familyID uint) *gomail.Message {
	//TODO send link to frontend
	url := Env.AppURL + "/families/" + fmt.Sprint(familyID)

	m := gomail.NewMessage()
	m.SetHeader("From", Env.MailSender)
	m.SetHeader("To", email)
	m.SetHeader("Subject", "Invite to family")
	m.SetBody("text/html", "POST "+url)
	return m
}
