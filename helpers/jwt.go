package helpers

import (
	"fmt"
	"strconv"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type JWTResponse struct {
	Token string `json:"token" example:"eyJhb.eyJleHAiO.7XJEAvxHk"`
}

func MakeAuthToken(id string, role string) JWTResponse {
	claims := &jwt.MapClaims{
		"exp":  time.Now().Add(time.Hour).Unix(),
		"iat":  time.Now().Unix(),
		"jti":  id,
		"role": role,
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), claims)
	tokenstring, err := token.SignedString([]byte(Env.JWTSecret))
	if err != nil {
		Log.Fatalln(err)
	}
	return JWTResponse{Token: tokenstring}
}

func MakeEmailVerificationToken(email string) string {
	claims := &jwt.MapClaims{
		"exp":   time.Now().Add(time.Hour).Unix(),
		"iat":   time.Now().Unix(),
		"email": email,
		"type":  "email_check",
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), claims)
	tokenstring, err := token.SignedString([]byte(Env.JWTSecret))
	if err != nil {
		Log.Fatalln(err)
	}
	return tokenstring
}

func CheckEmailVerificationToken(tokenString string) (email string, err error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(Env.JWTSecret), nil
	})

	if err != nil {
		return "", err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims["email"].(string), err
	}
	return "", err
}

func GetUintID(c *gin.Context) uint {
	id, _ := c.Get("id")
	uID, err := strconv.ParseUint(id.(string), 10, 64)
	if err != nil {
		NewError(c, 403, err, "invalid id")
		return 0
	}
	return uint(uID)
}
