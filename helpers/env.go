package helpers

import (
	"github.com/caarlos0/env"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

type config struct {
	GinMode    string `env:"GIN_MODE" envDefault:"release"`
	ListenAddr string `env:"LISTEN" envDefault:"127.0.0.1:8080"`
	AppURL     string `env:"APP_URL" envDefault:"http://localhost:8080"`
	LogFormat  string `env:"LOG_FORMAT" envDefault:"text"`
	LogLevel   string `env:"LOG_LEVEL" envDefault:"info"`

	DbHost string `env:"DB_HOST" envDefault:"127.0.0.1"`
	DbPort string `env:"DB_PORT" envDefault:"5432"`
	DbName string `env:"DB_NAME" envDefault:"pgdb"`
	DbUser string `env:"DB_USERNAME" envDefault:"pguser"`
	DbPass string `env:"DB_PASSWORD" envDefault:"pgpass"`

	MailSender string `env:"MAIL_SENDER"`
	MailHost   string `env:"MAIL_HOST"`
	MailPort   string `env:"MAIL_PORT"`
	MailUser   string `env:"MAIL_USERNAME"`
	MailPass   string `env:"MAIL_PASSWORD"`

	JWTSecret string `env:"JWT_SECRET"`
}

var Env = LoadEnv()

func init() {
	gin.SetMode(Env.GinMode)
}

func LoadEnv() config {
	var envconf = config{}

	// When app running from subfolder
	godotenv.Load("../.env")
	godotenv.Load(".env")

	// Parse environment into struct
	env.Parse(&envconf)

	return envconf
}
